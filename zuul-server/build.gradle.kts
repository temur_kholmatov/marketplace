import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("jvm")
    kotlin("plugin.spring") version "1.3.72"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://repo.spring.io/snapshot") }
    maven { url = uri("https://repo.spring.io/milestone") }
}

dependencies {
    implementation(Libs.spring_boot_starter_webflux) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
    implementation(Libs.spring_boot_starter_security)
    implementation(Libs.netflix_eureka_client)
    implementation(Libs.netflix_zuul)
    implementation(Libs.jjwt)
    implementation(project(":shared:jwt-config"))
    implementation(kotlin("stdlib"))
    testImplementation(Libs.spring_boot_test) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
}

dependencyManagement {
    imports {
        mavenBom(Libs.spring_cloud_dependencies)
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
