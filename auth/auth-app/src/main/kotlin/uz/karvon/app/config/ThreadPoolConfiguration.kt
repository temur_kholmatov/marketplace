package uz.karvon.app.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.fix.dynamic.property.api.DynamicProperty
import ru.fix.dynamic.property.api.annotation.PropertyId
import uz.karvon.threadpool.CoroutineExecutionContext
import uz.karvon.threadpool.CoroutineExecutionContextSettings
import ru.fix.aggregating.profiler.Profiler


@Configuration
open class ThreadPoolConfiguration(
        @PropertyId("auth-app.thread.pool.repo.size")
        private val repoPoolSettings: DynamicProperty<Int> = DynamicProperty.of(
                CoroutineExecutionContextSettings().contextSize
        )
) {

    @Bean
    open fun repoPool(
            profiler: Profiler
    ) = CoroutineExecutionContext("repo-pool", repoPoolSettings, profiler)
}
