package uz.karvon.app.config

import org.mybatis.spring.annotation.MapperScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import uz.karvon.app.repo.UserRepository
import uz.karvon.app.repo.mybatis.MbUserRepository
import uz.karvon.app.repo.mybatis.mapper.UserMapper
import uz.karvon.threadpool.CoroutineExecutionContext

@Configuration
@MapperScan("uz.karvon.app.repo.mybatis.mapper")
class RepositoryConfiguration {
    @Bean
    fun userRepository(
            userMapper: UserMapper,
            repoPool: CoroutineExecutionContext
    ): UserRepository = MbUserRepository(userMapper, repoPool)
}