package uz.karvon.app.domain.services

import kotlinx.coroutines.runBlocking
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import uz.karvon.app.domain.model.AppUser
import uz.karvon.app.repo.UserRepository

@Service // It has to be annotated with @Service.
class AppUserDetailsService(private val userRepository: UserRepository) : UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails = runBlocking<UserDetails> {
        val appUser: AppUser? = userRepository.findUserByUsername(username)
        if (appUser != null) {
            val grantedAuthorities = AuthorityUtils
                    .commaSeparatedStringToAuthorityList("ROLE_" + appUser.role.code.toUpperCase())

            // The "User" class is provided by Spring and represents a model class for user to be returned by UserDetailsService
            // And used by auth manager to verify and check user authentication.
            User(appUser.username, appUser.password, grantedAuthorities)
        } else {
            throw UsernameNotFoundException("Username: $username not found")
        }
    }
}