package uz.karvon.app.domain.services

import org.apache.logging.log4j.kotlin.Logging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import ru.fix.ewp.lang.CodableEnum
import uz.karvon.app.domain.model.AppUser
import uz.karvon.app.repo.CreateUserResult
import uz.karvon.app.repo.UserRepository

@Service
class RegistrationService(private val userRepository: UserRepository) {
    companion object : Logging

    @Autowired
    lateinit var encoder: BCryptPasswordEncoder

    suspend fun register(id: Long, username: String, password: String, role: String): CreateUserResult {
        if (userRepository.findUserByUsername(username) == null) {
            return try {
                userRepository.create(AppUser(
                        id = id,
                        username = username,
                        password = encoder.encode(password),
                        role = CodableEnum.ofCode(role)
                ))
                CreateUserResult.Success
            } catch (e: DuplicateKeyException) {
                logger.warn("Duplicate key for create contractor with wallet. " +
                        "user_id = $id", e)
                CreateUserResult.Conflict
            }
        }
        return CreateUserResult.Conflict
    }
}