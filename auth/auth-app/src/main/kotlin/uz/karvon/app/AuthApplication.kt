package uz.karvon.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Import
import uz.karvon.app.config.*


@Import(
        DatabaseConfiguration::class,
        RepositoryConfiguration::class,
        MyBatisConfiguration::class,
        ProfilerConfiguration::class,
        ThreadPoolConfiguration::class
)
@SpringBootApplication
@EnableEurekaClient
class AuthApplication

fun main(args: Array<String>) {
    runApplication<AuthApplication>(*args)
}
