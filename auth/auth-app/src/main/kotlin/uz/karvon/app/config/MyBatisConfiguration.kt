package uz.karvon.app.config

import org.apache.ibatis.session.ExecutorType
import org.mybatis.spring.SqlSessionFactoryBean
import org.mybatis.spring.SqlSessionTemplate
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn
import org.springframework.context.annotation.Import
import uz.karvon.app.repo.mybatis.handlers.UserRoleHandler
import uz.karvon.database.liquibase.config.MarketplaceLiquibaseConfiguration
import javax.sql.DataSource

@Configuration
@Import(MarketplaceLiquibaseConfiguration::class)
class MyBatisConfiguration {

    @Bean
    @DependsOn(MarketplaceLiquibaseConfiguration.MARKETPLACE_LIQUIBASE)
    fun mybatisSqlSessionFactory(
            @Qualifier("authDataSource") dataSource: DataSource
    ): SqlSessionFactoryBean {
        val sessionFactory = SqlSessionFactoryBean()
        sessionFactory.setDataSource(dataSource)
        sessionFactory.setTypeHandlers(
                UserRoleHandler()
        )
        return sessionFactory
    }

    @Bean
    @DependsOn("mybatisSqlSessionFactory")
    fun mybatisSqlSessionTemplate(
            @Qualifier("mybatisSqlSessionFactory") mybatisSqlSessionFactory: SqlSessionFactoryBean
    ): SqlSessionTemplate {
        return SqlSessionTemplate(mybatisSqlSessionFactory.getObject(), ExecutorType.REUSE)
    }
}