package uz.karvon.app.repo.mybatis.handlers

import org.apache.ibatis.type.MappedTypes
import ru.fix.ewp.database.mybatis.handler.AbstractEnumTypeHandler
import uz.karvon.app.domain.model.UserRole

@MappedTypes(UserRole::class)
class UserRoleHandler : AbstractEnumTypeHandler<UserRole>() {
    override fun values(): Array<UserRole> {
        return UserRole.values()
    }
}