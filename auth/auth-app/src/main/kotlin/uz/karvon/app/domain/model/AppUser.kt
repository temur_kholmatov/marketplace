package uz.karvon.app.domain.model

import ru.fix.ewp.lang.CodableEnum

data class AppUser(
        val id: Long,
        val username: String,
        val password: String,
        val role: UserRole
)

enum class UserRole(override val code: String): CodableEnum {
    ADMIN("admin"),
    USER("user")
}