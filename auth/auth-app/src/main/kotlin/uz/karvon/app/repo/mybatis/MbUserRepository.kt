package uz.karvon.app.repo.mybatis

import uz.karvon.app.domain.model.AppUser
import uz.karvon.app.repo.UserRepository
import uz.karvon.app.repo.mybatis.mapper.UserMapper
import uz.karvon.threadpool.CoroutineExecutionContext

class MbUserRepository(
        private val userMapper: UserMapper,
        private val repoPool: CoroutineExecutionContext
): UserRepository {
    override suspend fun findUserByUsername(username: String): AppUser? = repoPool{
        userMapper.findByUsername(username)
    }

    override suspend fun create(user: AppUser) = repoPool{
        userMapper.create(user)
    }
}