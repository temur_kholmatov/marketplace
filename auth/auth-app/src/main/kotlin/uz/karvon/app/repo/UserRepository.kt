package uz.karvon.app.repo

import uz.karvon.app.domain.model.AppUser

interface UserRepository {
    suspend fun findUserByUsername(username: String): AppUser?

    suspend fun create(user: AppUser)

}

sealed class CreateUserResult {
    object Success : CreateUserResult()
    object Conflict : CreateUserResult()
}
