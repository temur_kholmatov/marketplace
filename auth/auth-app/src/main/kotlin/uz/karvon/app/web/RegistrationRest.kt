package uz.karvon.app.web

import kotlinx.coroutines.runBlocking
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import uz.karvon.app.domain.services.RegistrationService
import uz.karvon.app.repo.CreateUserResult

@RestController
class RegistrationRest(private val registrationService: RegistrationService) {
    @PostMapping("/auth/register")
    fun register(@RequestBody request: RegisterRequest): ResponseEntity<RegisterResponse> {
        val id = request.id
        val username = request.username
        val password = request.password
        val role = request.role

        val result = runBlocking {
            registrationService.register(
                    id = id,
                    username = username,
                    password = password,
                    role = role
            )
        }


        return when (result) {
            is CreateUserResult.Success -> {
                ResponseEntity.ok(
                        RegisterResponse.Accepted(
                                id = id,
                                username = username,
                                role = role
                        )
                )
            }
            is CreateUserResult.Conflict -> {
                ResponseEntity.status(HttpStatus.CONFLICT).build()
            }
        }
    }
}