package uz.karvon.app.web

import com.fasterxml.jackson.annotation.JsonProperty

data class RegisterRequest(
        @JsonProperty("id")
        val id: Long,
        @JsonProperty("username")
        val username: String,
        @JsonProperty("password")
        val password: String,
        @JsonProperty("role")
        val role: String
)

sealed class RegisterResponse(
        val status: String
) {
    class Accepted(
            id: Long,
            username: String,
            role: String
    ) : RegisterResponse(status = "accepted") {
        val data: RegisterResponseData = RegisterResponseData(
                id = id,
                username = username,
                role = role
        )
    }
}

data class RegisterResponseData(
        @get:JsonProperty("id")
        val id: Long,
        @get:JsonProperty("username")
        val username: String,
        @get:JsonProperty("role")
        val role: String
)