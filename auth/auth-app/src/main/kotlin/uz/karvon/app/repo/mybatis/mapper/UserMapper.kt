package uz.karvon.app.repo.mybatis.mapper

import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import uz.karvon.app.domain.model.AppUser

@Mapper
interface UserMapper {
    @Select("SELECT * FROM auth.user WHERE username = #{username}")
    fun findByUsername(@Param("username") username: String): AppUser?

    @Insert("INSERT INTO auth.user (id, username, password, role) VALUES (#{id}, #{username}, #{password}, #{role}::auth.user_role)")
    fun create(user: AppUser)
}