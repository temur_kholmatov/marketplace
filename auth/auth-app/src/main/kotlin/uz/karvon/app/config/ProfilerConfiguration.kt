package uz.karvon.app.config

import org.apache.logging.log4j.kotlin.Logging

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.fix.aggregating.profiler.AggregatingProfiler
import ru.fix.aggregating.profiler.PercentileSettings

//import org.springframework.beans.factory.annotation.Value
//import org.springframework.context.annotation.DependsOn
//import ru.fix.aggregating.profiler.Profiler
//import ru.fix.aggregating.profiler.prometheus.PrometheusMetricsReporter
//import ru.fix.dynamic.property.api.DynamicProperty
//import ru.fix.dynamic.property.api.annotation.PropertyId
//import uz.karvon.threadpool.ThreadPoolGuardSettings

@Configuration
class ProfilerConfiguration {
    companion object : Logging

//    @Value("\${server_id}")
//    var serverId: Long? = null

//    @PropertyId("auth-app.profiler.percentile.settings")
    private val profilerPercentileSettings = PercentileSettings()

//    @PropertyId("bookkeeper.rest.profile.settings")
//    private val profiledWebFilterSettings = DynamicProperty.of(ProfiledWebFilterSettings())

//    @PropertyId("bookkeeper.common.thread.pool.guard.settings")
//    private var commonPoolGuardSettings = DynamicProperty.of(ThreadPoolGuardSettings())

    @Bean
    open fun profiler() = AggregatingProfiler(profilerPercentileSettings)

//    @Bean
//    open fun profiledWebFilter(profiler: Profiler) = ProfiledWebFilter(profiler, profiledWebFilterSettings)
//
//    @Bean
//    open fun controllerProfilerAspect() = ControllerProfilerAspect(profiledWebFilterSettings)

//    @Bean(destroyMethod = "close")
//    @DependsOn("profiler")
//    open fun prometheusMetricsReporter(profiler: Profiler): PrometheusMetricsReporter {
//        requireNotNull(serverId) {
//            "serverId is not defined"
//        }
//
//        return PrometheusMetricsReporter(
//                profiler.createReporter(),
//                mapOf(
//                        "serverId" to serverId.toString(),
//                        "app" to "bookkeeper"
//                )
//        )
//    }

//    @Bean
//    open fun commonThreadPoolGuard(profiler: Profiler): CommonThreadPoolGuard {
//        return CommonThreadPoolGuard(
//                profiler,
//                commonPoolGuardSettings.map { Schedule.withDelay(it.checkDelay.toLong()) },
//                commonPoolGuardSettings.map { it.queueThreshold }
//        ) { queueSize: Int?, dump: String? ->
//            logger.error(
//                    "CommonPool queue threshold reached." +
//                            " Queue size: $queueSize, limit: ${commonPoolGuardSettings.get().queueThreshold}\n" +
//                            "Thread dump:\n$dump"
//            )
//        }
//    }

}