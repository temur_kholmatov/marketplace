package uz.karvon.app.domain.services

import io.mockk.*
import org.junit.jupiter.api.BeforeAll
import kotlin.random.Random
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import uz.karvon.app.domain.model.AppUser
import uz.karvon.app.domain.model.UserRole
import uz.karvon.app.repo.CreateUserResult
import uz.karvon.app.repo.UserRepository
import io.kotlintest.shouldBe
import mu.KotlinLogging.logger
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.BeforeEach
import uz.karvon.app.domain.util.UserUtil.Companion.initAdmin
import uz.karvon.app.domain.util.UserUtil.Companion.initRandomUser

class RegistrationServiceTest {
    private lateinit var service: RegistrationService
    private lateinit var userRepository: UserRepository
    private var userAdmin: AppUser = initAdmin()
    private var userSimple: AppUser = initRandomUser()

    companion object : Logging

    @BeforeEach
    fun setUp() {
        userRepository = mockk()
        service = RegistrationService(
                userRepository = userRepository
        )
    }

    @Test
    fun `регистрация администратора`() = runBlocking {
        val approved = CreateUserResult.Success

        coEvery {
            service.register(
                    userAdmin.id,
                    userAdmin.username,
                    userAdmin.password,
                    userAdmin.role.toString()
            )
        } returns approved
        logger.info(approved.toString())

        approved shouldBe CreateUserResult.Success
    }


}