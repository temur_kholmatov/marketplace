package uz.karvon.app.domain.util

import com.github.javafaker.Faker
import com.github.javafaker.service.FakeValuesService
import com.github.javafaker.service.RandomService
import uz.karvon.app.domain.model.AppUser
import uz.karvon.app.domain.model.UserRole
import java.util.*

open class UserUtil {
    companion object {
        val faker = Faker()
        val random = Random()
        val fakeValueServices = FakeValuesService(Locale("en-GB"), RandomService())

        fun initRandomUser(): AppUser {
            return AppUser(
                    id = random.nextLong(),
                    username = faker.name().username(),
                    password = "samplePass",
                    role = UserRole.USER
            )
        }

        fun initAdmin(): AppUser {
            return AppUser(
                    id = random.nextLong(),
                    username = faker.name().username(),
                    password = "admin",
                    role = UserRole.ADMIN
            )
        }
    }
}

