import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("jvm")
    kotlin("plugin.spring") version "1.3.72"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://repo.spring.io/snapshot") }
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://jitpack.io") }
}

buildscript {
    dependencies {
        classpath(Libs.postgresql)
    }
}

dependencies {
    implementation(Libs.spring_boot_starter_webflux) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
    implementation(Libs.spring_boot_starter_security)
    implementation(Libs.spring_jdbc)

    implementation(Libs.netflix_eureka_client)
    implementation(Libs.netflix_zuul)

    implementation(Libs.jjwt)
    implementation(Libs.dynamic_property)
    implementation(Libs.log4j_kotlin)

    implementation(Libs.mybatis)
    implementation(Libs.mybatis_spring)
    implementation(Libs.mybatis_typehandlers)

    implementation(project(":shared:database"))
    implementation(project(":shared:jwt-config"))
    implementation(project(":shared:thread-pool"))

    implementation(project(":inventory:inventory-database"))
    implementation(Libs.faker) { exclude ("org.yaml") }
    implementation("org.yaml:snakeyaml:1.26")
    implementation(kotlin("stdlib"))
    implementation(Libs.kotlinx_coroutines_core)
    implementation(Libs.kotlinx_coroutines_reactor)

    implementation(Libs.aggregating_profiler)
    implementation(Libs.aggregating_profiler_prometheus)

    testImplementation(Libs.mockk)
    testImplementation(Libs.junit_api)
    testRuntimeOnly(Libs.junit_engine)

    testImplementation(Libs.kotlintest)


    testImplementation(Libs.spring_boot_test) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
}

dependencyManagement {
    imports {
        mavenBom(Libs.spring_cloud_dependencies)
    }
}


tasks.withType<Test> {
    useJUnitPlatform()
}
//tasks.test{
//    useJUnitPlatform()
//}
//
//tasks.withType<KotlinCompile> {
//    kotlinOptions {
//        freeCompilerArgs = listOf("-Xjsr305=strict")
//        jvmTarget = "11"
//    }
//}
