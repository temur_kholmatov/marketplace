#!/usr/bin/env bash

export DOCKER_TAG=local
#export REGISTRY=karvon.uz

echo "Stopping apps..."
docker-compose kill
docker-compose -f docker-compose.karvon.yml kill
docker rm $(docker ps -a -q)

sleep 5

docker container prune -f && docker network prune -f && docker volume prune -f

echo -e "\033[0;32m Cleanup is finished. \033[0m"