package uz.karvon.database.liquibase.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import uz.karvon.database.liquibase.integration.SpringLiquibase
import javax.sql.DataSource


/**
 * Base configuration for Liquibase.
 */
@Configuration
open class MarketplaceLiquibaseConfiguration {

    @Autowired
    lateinit var marketplaceDataSource: DataSource

    @Bean(name = [MARKETPLACE_LIQUIBASE])
    open fun liquibase(): SpringLiquibase {
        return SpringLiquibase().also {
            it.dataSource = marketplaceDataSource
            it.changeLog = "classpath:liquibase/db.master.xml"
            it.liquibaseSchema = "public"
        }
    }

    companion object {
        const val MARKETPLACE_LIQUIBASE = "marketplaceLiquibase"
    }
}

