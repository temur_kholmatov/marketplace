plugins {
    java
    kotlin("jvm")
}

buildscript {
    dependencies {
        classpath(Libs.postgresql)
    }
}

dependencies {

    implementation(Libs.log4j_kotlin)

    implementation(project(":shared:database-liquibase-spring"))
    implementation(Libs.commons_dbcp2)
    implementation(Libs.dynamic_property) {
        //will be fixed in nearest dynamic property release
        exclude("org.slf4j")
    }

    implementation(Libs.kotlin_jdk8)
    implementation(Libs.kotlin_stdlib)
    implementation(Libs.kotlin_reflect)
    implementation(Libs.postgresql)

    testImplementation(Libs.testcontainers)
    testImplementation(Libs.testcontainers_postgres)
    testImplementation(Libs.junit_engine)
    testImplementation(Libs.junit_api)
}