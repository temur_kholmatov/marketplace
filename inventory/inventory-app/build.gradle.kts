import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("jvm")
    kotlin("plugin.spring") version "1.3.72"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://repo.spring.io/snapshot") }
    maven { url = uri("https://repo.spring.io/milestone") }
}

buildscript {
    dependencies {
        classpath(Libs.postgresql)
    }
}

dependencies {
    implementation(Libs.spring_boot_starter_webflux) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
    implementation(Libs.netflix_eureka_client)
    implementation(Libs.dynamic_property)
    implementation(Libs.log4j_kotlin)
    implementation(Libs.spring_jdbc)
    implementation(project(":shared:database"))
    implementation(project(":inventory:inventory-database"))
    implementation(kotlin("stdlib"))
    testImplementation(Libs.spring_boot_test) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
}

dependencyManagement {
    imports {
        mavenBom(Libs.spring_cloud_dependencies)
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
