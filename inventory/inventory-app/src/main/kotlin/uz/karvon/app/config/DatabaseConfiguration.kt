package uz.karvon.app.config

import org.apache.logging.log4j.kotlin.Logging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.support.TransactionTemplate
import ru.fix.dynamic.property.api.DynamicProperty
import ru.fix.dynamic.property.api.annotation.PropertyId
import uz.karvon.database.settings.DataSourceBuilder
import uz.karvon.database.settings.DbSettings
import javax.sql.DataSource

private const val DRIVER_CLASS_NAME = "org.postgresql.Driver"

@Configuration
class DatabaseConfiguration {
    companion object : Logging

    @Value("\${db-username}")
    private lateinit var datasourceUsername: String

    @Value("\${db-password}")
    private lateinit var datasourcePassword: String

    @PropertyId("marketplace.db.settings")
    private val dbSettings = DynamicProperty.of(
            DbSettings(
                    jdbcUrl = "jdbc:postgresql://db-market:5432/marketplace",
                    connectionPoolSize = 10
            )
    )

    @Bean
    fun marketplaceDataSource(): DataSource {
        val dataSource = DataSourceBuilder(dbSettings)
                .buildDataSource(
                        DRIVER_CLASS_NAME,
                        datasourceUsername,
                        datasourcePassword
                )

        logger.info(
                "Create dbcp2 connection pool.\n" +
                        "Connection pool size: ${dbSettings.get().connectionPoolSize}\n" +
                        "Url: ${dbSettings.get().jdbcUrl}"
        )

        return dataSource
    }

    @Bean
    fun transactionManager(
            @Qualifier("marketplaceDataSource") dataSource: DataSource
    ): PlatformTransactionManager {
        return DataSourceTransactionManager(dataSource)
    }

    @Bean
    fun transactionTemplate(transactionManager: PlatformTransactionManager): TransactionTemplate {
        return TransactionTemplate(transactionManager)
    }
}