package uz.karvon.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Import
import uz.karvon.app.config.DatabaseConfiguration
import uz.karvon.database.liquibase.config.MarketplaceLiquibaseConfiguration

@Import(
        DatabaseConfiguration::class,
        MarketplaceLiquibaseConfiguration::class
)
@SpringBootApplication
@EnableEurekaClient
class InventoryApplication

fun main(args: Array<String>) {
    runApplication<InventoryApplication>(*args)

    println("hello world!")
}

