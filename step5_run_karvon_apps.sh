#!/usr/bin/env bash

set -e

# Verify args
APP="undefined"
apps=( "inventory" )

if [[ -n "${1}" ]] && [[ ! " ${apps[@]} " =~ " ${1} " ]]; then
    echo echo "Invalid argument. Valid args: ${apps[*]}"
    exit 1
else
    APP="${1}"
fi

# Set env variables
export DOCKER_TAG=local
export REGISTRY=registry.karvon.uz

# if $APP is not empty
if [[ -n "$APP" ]]; then
    docker-compose -f docker-compose.karvon.yml kill "$APP"
    docker-compose -f docker-compose.karvon.yml up -d "$APP"
else
    docker-compose -f docker-compose.karvon.yml kill
    docker-compose -f docker-compose.karvon.yml up -d eureka-server
    docker-compose -f docker-compose.karvon.yml up -d zuul-server
    docker-compose -f docker-compose.karvon.yml up -d inventory auth
fi

echo -e "\033[0;32m SERVICES ARE UP. \033[0m"
#echo 'Portainer GUI is available at http://localhost:19000/#/dashboard'
#python -mwebbrowser http://localhost:19000/#/dashboard || true
