package uz.karvon.threadpool

data class ThreadPoolGuardSettings(
        val checkDelay: Int = 10000,
        val queueThreshold: Int = 10000
)