package uz.karvon.threadpool

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.fix.aggregating.profiler.Profiler
import ru.fix.dynamic.property.api.DynamicProperty
import ru.fix.stdlib.concurrency.threads.NamedExecutors


data class CoroutineExecutionContextSettings(
        val contextSize: Int = 12
)

class CoroutineExecutionContext(
        threadPoolName: String,
        contextSize: DynamicProperty<Int>,
        profiler: Profiler
) {

    private val dispatcher = NamedExecutors.newDynamicPool(
            threadPoolName,
            contextSize,
            profiler
    ).asCoroutineDispatcher()

    suspend operator fun <T> invoke(block: suspend CoroutineScope.() -> T): T = withContext(dispatcher, block)
}