plugins{
    java
    kotlin("jvm")
}

dependencies{
    implementation(Libs.dynamic_property)
    implementation(Libs.dynamic_property_std_source)
    implementation(Libs.dynamic_property_spring)
    implementation(Libs.dynamic_property_zk)
    implementation(Libs.stdlib_concurrency)

    implementation(Libs.aggregating_profiler)
    implementation(Libs.aggregating_profiler_prometheus)

    implementation(Libs.kotlinx_coroutines_core)
    implementation(Libs.kotlinx_coroutines_reactor)

    // aspect
    implementation(Libs.aspectj_rt)
    implementation(Libs.aspectj_weaver)

    // spring
    implementation(Libs.spring_context)

}