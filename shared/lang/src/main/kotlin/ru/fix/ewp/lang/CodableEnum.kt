package ru.fix.ewp.lang

interface CodableEnum {
    val code: String

    companion object {

        inline fun <reified T> getByCode(code: String): T? where T : Enum<T>, T : CodableEnum {
            return enumValues<T>().firstOrNull { it.code.equals(code, ignoreCase = true) }
        }

        inline fun <reified T> isPresentForCode(code: String): Boolean where T : Enum<T>, T : CodableEnum {
            return getByCode<T>(code) != null
        }

        inline fun <reified T> ofCode(code: String): T where T : Enum<T>, T : CodableEnum {
            return getByCode(code)
                ?: throw IllegalCodableEnumCodeException(
                    "No enum value for code $code was found. " +
                            "Available codes: ${enumValues<T>().map { it.code }}"
                )
        }
    }

}
