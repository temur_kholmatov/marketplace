package ru.fix.ewp.lang

class IllegalCodableEnumCodeException(override val message: String) : IllegalArgumentException()