package ru.fix.ewp.lang

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource

class CodableEnumTest {

    @ParameterizedTest
    @ValueSource(strings = ["playstation", "xbox", "Playstation", "xBox"])
    fun `enum is preset for code`(console: String) {
        assertTrue { CodableEnum.isPresentForCode<GameConsole>(console) }
    }

    @ParameterizedTest
    @ValueSource(strings = ["psp", "sega"])
    fun `enum is not preset for code`(gameConsoleCode: String) {
        assertFalse { CodableEnum.isPresentForCode<GameConsole>(gameConsoleCode) }
    }

    @ParameterizedTest
    @CsvSource("playstation,PLAYSTATION", "xbox,XBOX")
    fun `create enum from code`(gameConsoleCode: String, gameConsole: GameConsole) {
        assertEquals(CodableEnum.ofCode<GameConsole>(gameConsoleCode), gameConsole)
    }


    @ParameterizedTest
    @ValueSource(strings = ["psp", "sega", "playstation3"])
    fun `not create enum from wrong code`(gameConsoleCode: String) {
        assertThrows<IllegalCodableEnumCodeException> {
            CodableEnum.ofCode<GameConsole>(gameConsoleCode)
        }
    }
}

enum class GameConsole(override val code: String) : CodableEnum {
    PLAYSTATION("playstation"),
    XBOX("xbox");
}