plugins {
    java
    kotlin("jvm")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(Libs.kotlin_jdk8)
    implementation(Libs.kotlin_stdlib)

    testImplementation(Libs.junit_api)
    testImplementation(Libs.junit_engine)
    testImplementation(Libs.junit_params)
}