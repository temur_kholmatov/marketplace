plugins {
    java
    kotlin("jvm")
}

repositories {
    mavenCentral()
}

dependencies {
    api(Libs.liquibase_core) {
        exclude("ch.qos.logback", "logback-classic")
    }
    api(Libs.spring_context)
    implementation(kotlin("stdlib"))
}