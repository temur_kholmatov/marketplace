package uz.karvon.database.liquibase.integration

import liquibase.exception.UnexpectedLiquibaseException
import java.io.IOException
import java.net.URLClassLoader

class SpringLiquibase : liquibase.integration.spring.SpringLiquibase() {

    override fun createResourceOpener(): SpringResourceOpener {
        return EwpSpringResourceOpener(getChangeLog())
    }

    inner class EwpSpringResourceOpener(parentFile: String) : SpringResourceOpener(parentFile) {

        // Workaround for
        // https://liquibase.jira.com/browse/CORE-3001,
        // https://liquibase.jira.com/browse/CORE-3121,
        // workaround probably should be removed in liquibase v3.9+ version after release,
        // 3.8 and lower still have problem
        // see also https://github.com/dCache/dcache/commit/ff52847c4f1b305d3a3f174c4c5663388a820ba2
        override fun init() {
            try {
                val classLoader = toClassLoader()
                    ?: return

                if (classLoader is URLClassLoader) {
                    classLoader.urLs.forEach {
                        addRootPath(it)
                    }
                }
                classLoader.getResources("").iterator().forEach {
                    addRootPath(it)
                }
            } catch (e: IOException) {
                throw UnexpectedLiquibaseException(e)
            }
        }
    }
}