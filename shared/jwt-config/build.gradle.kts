plugins {
    java
//    id("org.springframework.boot")
//    id("io.spring.dependency-management")
    kotlin("jvm")
//    kotlin("plugin.spring") version "1.3.72"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(Libs.spring_boot_starter_webflux) {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
    }
    implementation(kotlin("stdlib"))
}