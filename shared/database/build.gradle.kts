plugins {
    java
    kotlin("jvm")
}

repositories {
    mavenCentral()
}

dependencies {
    api(project(":shared:lang"))
    implementation(Libs.kotlin_jdk8)
    implementation(Libs.kotlin_stdlib)
    implementation(Libs.kotlin_reflect)
    implementation(Libs.log4j_kotlin)
    implementation(Libs.jackson_jsr310)
    implementation(Libs.jackson_kotlin)

    api(Libs.commons_dbcp2)
    implementation(Libs.dynamic_property)
    implementation(Libs.mybatis)
}