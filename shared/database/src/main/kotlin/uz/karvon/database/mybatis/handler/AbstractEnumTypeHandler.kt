package ru.fix.ewp.database.mybatis.handler

import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.TypeHandler
import ru.fix.ewp.lang.CodableEnum
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

abstract class AbstractEnumTypeHandler<T : CodableEnum> : TypeHandler<T> {

    @Throws(SQLException::class)
    override fun setParameter(ps: PreparedStatement, i: Int, parameter: T?, jdbcType: JdbcType?) {
        if (parameter != null) {
            ps.setString(i, parameter.code)
        } else {
            ps.setNull(i, jdbcType?.TYPE_CODE ?: error("Type code should be not null"))
        }
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnName: String): T? {
        return getByCode(rs.getString(columnName))
    }

    @Throws(SQLException::class)
    override fun getResult(rs: ResultSet, columnIndex: Int): T? {
        return getByCode(rs.getString(columnIndex))
    }

    @Throws(SQLException::class)
    override fun getResult(cs: CallableStatement, columnIndex: Int): T? {
        return getByCode(cs.getString(columnIndex))
    }

    private fun getByCode(code: String?): T? {
        code ?: return null
        return values()
            .firstOrNull { it.code.equals(code, ignoreCase = true) }
            ?: throw IllegalArgumentException("No enum value with code $code was found")
    }


    abstract fun values(): Array<T>
}