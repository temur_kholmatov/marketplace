package ru.fix.ewp.database.mybatis.handler

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.IOException

object DatabaseJsonMarshaller {

    private val mapper: ObjectMapper = ObjectMapper()
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .registerModule(KotlinModule())
        .registerModule(JavaTimeModule())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

    fun marshall(serializedObject: Any): String {
        try {
            return mapper.writeValueAsString(serializedObject)
        } catch (e: JsonProcessingException) {
            throw DatabaseJsonSerializeException("Failed to marshalling pojo. Object details: $serializedObject", e)
        }
    }

    /**
     * Note about nullable result:
     * the only "null" will be deserialized to null,
     * in other cases deserialization will return object or throws JSonSerializeException
     */
    inline fun <reified T : Any> unmarshall(json: String): T? {
        return unmarshall(json, T::class.java)
    }

    fun <T> unmarshall(json: String, targetType: Class<T>): T {
        try {
            return mapper.readValue(json, targetType)
        } catch (e: IOException) {
            throw DatabaseJsonSerializeException("Failed to unmarshall json text to type $targetType. JSon: $json", e)
        }

    }
}