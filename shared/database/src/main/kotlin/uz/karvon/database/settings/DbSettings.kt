package uz.karvon.database.settings

import org.apache.commons.dbcp2.BasicDataSource
import org.apache.commons.dbcp2.PoolableConnectionFactory
import org.apache.commons.pool2.impl.BaseObjectPoolConfig
import org.apache.logging.log4j.kotlin.Logging
import ru.fix.dynamic.property.api.DynamicProperty

data class DbSettings(
        val jdbcUrl: String,
        val connectionPoolSize: Int,
        val validationQuery: String = "select 1;",
        /**
         * If non-null, the value of this Integer property determines the query timeout that will be used for Statements
         * created from connections managed by the pool. null means that the driver default will be used.
         */
        val defaultQueryTimeoutSec: Int? = null,
        /**
         * The maximum number of milliseconds that the pool will wait (when there are no available connections)
         * for a connection to be returned before throwing an exception, or -1 (or null) to wait indefinitely.
         */
        val maxWaitMillis: Long? = null
)

class DataSourceBuilder(private val dbSettings: DynamicProperty<DbSettings>) {

    fun buildDataSource(driverClassName: String, username: String, password: String): BasicDataSource =
            DynamicChangeConnectionTimeoutDataSource(dbSettings).also {
                it.driverClassName = driverClassName
                it.username = username
                it.password = password

            }
}


class DynamicChangeConnectionTimeoutDataSource(dbSettings: DynamicProperty<DbSettings>) : BasicDataSource() {
    companion object : Logging

    private val settingsSubscription = dbSettings
            .createSubscription()
            .setAndCallListener { _, settings ->
                this.defaultQueryTimeout = settings.defaultQueryTimeoutSec
                this.maxWaitMillis = settings.maxWaitMillis
                        ?: BaseObjectPoolConfig.DEFAULT_MAX_WAIT_MILLIS
            }

    init {
        this.url = dbSettings.get().jdbcUrl
        this.maxIdle = dbSettings.get().connectionPoolSize
        this.maxTotal = dbSettings.get().connectionPoolSize
        this.validationQuery = dbSettings.get().validationQuery
    }

    override fun setDefaultQueryTimeout(defaultQueryTimeoutSeconds: Int?) {
        super.setDefaultQueryTimeout(defaultQueryTimeoutSeconds)

        // Изменение настройки dataSource.defaultQueryTimeout не влияет на уже используемые ConnectionFactory,
        // которые продолжают создавать соединения со старым значением таймаута.
        // Обновляем значение явно.
        val connectionPool = connectionPool
                ?: return // nothing to do, connectionPool wasn't created yet

        val connectionFactory = connectionPool.factory as? PoolableConnectionFactory
        if (connectionFactory == null) {
            logger.error(
                    "cannot update defaultQueryTimeout," +
                            " connectionFactory is null or is not PoolableConnectionFactory"
            )
            return
        }

        connectionFactory.defaultQueryTimeout = defaultQueryTimeoutSeconds
    }

    override fun close() {
        settingsSubscription.close()
        super.close()
    }
}
