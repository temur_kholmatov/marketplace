package ru.fix.ewp.database.mybatis.handler

class DatabaseJsonSerializeException(
    s: String,
    e: Throwable
) : Throwable(s, e)
