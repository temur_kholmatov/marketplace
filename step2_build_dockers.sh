#!/usr/bin/env bash

set -e

# Verify args
APP="${1}"
apps=( "inventory" "payments" )

if [[ -n "$APP" ]] && [[ ! " ${apps[@]} " =~ " $APP " ]]; then
    echo "Invalid argument. Valid args: ${apps[*]}"
    exit 1
fi

# Set env variables
export DOCKER_TAG=local
export REGISTRY=registry.karvon.uz

# Build docker images
# if $APP is not empty
if [[ -n "$APP" ]]; then
    docker-compose -f docker-compose.karvon.yml build "$APP"
else
    docker-compose -f docker-compose.karvon.yml build
fi

echo -e "\033[0;32m DOCKER IMAGES ARE BUILT SUCCESSFULLY. \033[0m"
