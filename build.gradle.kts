import org.asciidoctor.gradle.AsciidoctorTask
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


buildscript {
    repositories {
        jcenter()
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }
    dependencies {
        //Kotlin dependencies for gradle plugins
        //Different from project runtime
        classpath(Libs.gradle_kotlin_stdlib)
        classpath(Libs.gradle_kotlin_jdk8)
        classpath(Libs.gradle_kotlin_reflect)

        classpath(Libs.release_plugin)
//        classpath(Libs.jruby_gradle_plugin)
        classpath(Libs.asciidoctor_gems_plugin)
        classpath(Libs.asciidoctor_plugin)
    }
}

///**
// * Project configuration by properties and environment
// */
//fun envConfig() = object : kotlin.properties.ReadOnlyProperty<Any?, String?> {
//    override fun getValue(thisRef: Any?, property: kotlin.reflect.KProperty<*>): String? =
//            if (ext.has(property.name)) {
//                ext[property.name] as? String
//            } else {
//                System.getenv(property.name)
//            }
//}
//
//val repositoryUser by envConfig()
//val repositoryPassword by envConfig()
//val repositoryUrl by envConfig()
//val testXmx by envConfig()

plugins {
    kotlin("jvm") version Vers.gradle_kotlin apply false
    id("org.asciidoctor.convert") version Vers.asciidoctor
    id("org.springframework.boot") version Vers.spring_boot
}

subprojects {
    group = "uz.karvon"
    version = "1.0"

    apply {
        plugin("maven-publish")
        plugin("java")
    }

    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
    }

    tasks {

        withType<JavaCompile> {
            options.compilerArgs.add("-Xlint:all")
        }

        withType<KotlinCompile> {
            kotlinOptions.jvmTarget = "11"
            kotlinOptions.allWarningsAsErrors = true
        }

        withType<Jar> {
            archiveFileName.set("market-${project.name}.jar")
        }

        withType<Test> {
            useJUnitPlatform()

            jvmArgs = listOf("-Xmx2048m")

            testLogging {
                events(TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.SKIPPED)
                showStandardStreams = true
                exceptionFormat = TestExceptionFormat.FULL
            }
        }

        // show all subprojects dependencies `./gradlew allDeps`
        task("allDeps", DependencyReportTask::class) {}
    }
}

tasks {
    withType<AsciidoctorTask> {
        requires("asciidoctor-diagram")

        sourceDir = project.file("asciidoc")
        resources(closureOf<CopySpec> {
            from("asciidoc")
            include("**/*.png")
        })

        doLast {
            copy {
                from(outputDir.resolve("html5"))
                into(project.file("docs"))
                include("**/*.html", "**/*.png", "**/*.svg")
            }
        }
    }
}
