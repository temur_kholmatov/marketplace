#!/usr/bin/env bash

set -e

# Verify args
APP="undefined"
#NOZKSEED=false
apps=( "db-market" )

if [[ -n "${1}" ]] && [[ ! " ${apps[@]} " =~ " ${1} " ]]; then
    echo "Invalid argument. Valid args: ${apps[*]}"
    exit 1
else
    APP="${1}"
fi

# Set env variables
export DOCKER_TAG=local
export REGISTRY=registry.karvon.uz
export CURRENT_UID=$(id -u)
export CURRENT_GID=$(id -g)


# Run one app or all of them
if [[ " ${apps[@]} " =~ " $APP " ]]; then
    docker-compose kill "$APP"
    docker system prune -f --volumes
    docker-compose pull "$APP"  || true
    docker-compose up -d "$APP"
    echo "$APP is up."
else
    # First, stop all vasp containers.
    docker-compose kill

    # Stop all infra containers.
    docker-compose stop db-market 2> /dev/null || true

    # Delete old containers, networks and volumes
    docker system prune -f --volumes

    docker-compose pull || true

    # Run portainer
#    docker-compose up -d portainer

    # Run zookeeper, zk-web
#    docker-compose up -d zookeeper zk-web

    docker-compose up -d db-market
fi

#echo -e "\033[0;32m INFRA SERVICES ARE UP. IF YOU NEED BOOTSTRAPPING, THEN RUN STEP 4 (script step4_bootstrap.sh). \033[0m"
#echo -e "\033[0;32m IF YOU NEED EWP SERVICES ALSO RUNNING IN DOCKER (WITHOUT IDEA), THEN RUN STEP 5 (script step5_run_ewp_apps.sh). \033[0m"
#echo 'Portainer GUI is available at http://localhost:19000/#/dashboard'
#python -mwebbrowser http://localhost:19000/#/dashboard || true
