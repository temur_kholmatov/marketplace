# marketplace

## Description
Given a marketplace platform is done for thesis purposes only.

## Architecture
### Technology Stack
In the project given we are using standard enterprise JVM stack which consists of following technologies:
- [Spring](https://spring.io/) - for service backend
- [Liquibase](https://www.liquibase.org/) - for DB migrations
- [MyBatis](https://mybatis.org/mybatis-3/) - Java persistence framework that couples objects with stored procedures or SQL statements using an XML descriptor or annotations.
- [Docker](https://www.docker.com/) - for containerisation
- PostgreSQL - DBMS
- [Zuul](https://github.com/Netflix/zuul) - is a gateway service that provides dynamic routing, monitoring, resiliency, security, and more.
- [Eureka](https://github.com/Netflix/eureka) - is a REST (Representational State Transfer) based service that is primarily used in the AWS cloud for locating services for the purpose of load balancing and failover of middle-tier servers.
- JUnit - for testing


### Microservices Architectural Pattern
Current project holds to Microservices Architectural Pattern. The microservice architectural pattern is an approach to developing a single application as a suite of small services, each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API.

More on pattern read [here](https://martinfowler.com/articles/microservices.html#:~:text=In%20short%2C%20the%20microservice%20architectural,often%20an%20HTTP%20resource%20API.)

#### Modules of application
Currently project consits of following particles, which operate as stand alone services. These are:
- **Auth module** - works with Zuul, authenticates and authorises users.
- **Inventory** - module that responsible for product storage and following operations such as product purchase, overview, etc.
- **Payment** - handles money transactions between user and inventory service
- **Eureka Server** - usage of Netflix Eureka to map nodes(services) and provide load balancing.
- **Zuul Server* - usage of Netflix Zuul, that provides dynamic routing

### Database scheme
You can find our database scheme [here](https://drive.google.com/file/d/1B1FSfiBLu6sOGNKdyR56Q-DxQBwE-Bdo/view?usp=sharing)


## Branching Policy
In case you would like to contribute to the project, make sure that you have introduced issue first with following name:
`MP-###: Issue title`
and then create MR from issue. Make sure to follow MR format: `mp-###-issue-title`

## How to run:
1. Make sure that you have Java sdk installed. (Java 11 used in the project)
2. Make sure that you have docker and docker-compose installed
3. Simply run the script in bash:
    `./run_1235.sh`

## User Manual
For the better experience you should download the [Postman](https://www.postman.com/downloads/) to make following request.
Please, make sure that you have running server.

`api development in process`

### Sample Requests
|Desc | Verb | Route|
|-----|------|------|
|Signup| POST | `/user/create`|
|Login| POST | `/user/login` |
|Find user| GET | `/user/get/{id}`|
|Create product as Admin| POST | `/inventory/create/`|
|Get product as Admin | GET | `/inventory/productId={}` |
|Get product as User | GET | `/inventory/productId={}`|

