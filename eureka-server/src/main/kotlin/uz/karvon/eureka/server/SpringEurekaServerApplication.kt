package uz.karvon.eureka.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class SpringEurekaServerApplication

fun main(args: Array<String>) {
    runApplication<SpringEurekaServerApplication>(*args)
}
