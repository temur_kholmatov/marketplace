rootProject.name = "marketplace"

// shared
include("shared:database")
include("shared:database-liquibase-spring")
include("shared:jwt-config")
include("shared:lang")
include("shared:thread-pool")

// microservices
include("auth:auth-app")
//include("auth:auth-database")

include("inventory:inventory-app")
include("inventory:inventory-database")

include("eureka-server")

include("zuul-server")