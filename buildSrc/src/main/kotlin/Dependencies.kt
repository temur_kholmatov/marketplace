object Vers {
    //Third party

    // kotlin used by gradle runtime and gradle plugins
    // should be compatible with gradle version
    // https://docs.gradle.org/current/userguide/compatibility.html
    const val gradle_kotlin = "1.3.70"
    // kotlin used by source code and project runtime
    // https://search.maven.org/search?q=g:org.jetbrains.kotlin
    const val kotlin = "1.3.72"
    const val kotlin_coroutines = "1.3.5"
    // documentation engine for Kotlin
    const val dokkav = "0.10.1"
    const val log4j = "2.12.0"

    const val faker = "1.0.2"
    const val junit = "5.4.2"
    const val kotlintest = "3.4.2"
    const val cucumber_allure = "2.12.1"
    const val allure_plugin = "2.8.1"
    const val allure_cli = "2.13.1"
    const val cucumber_java = "4.7.2"
    const val cucumber_picocontainer = "4.7.2"

    const val rest_assured = "4.2.0"
    const val hamkrest = "1.7.0.0"
    const val asciidoctor = "1.5.9.2"
    const val jruby_gradle = "2.0.0"
    const val asciidoctor_gems = "3.2.0"
    const val guava = "28.0-jre"
    const val caffeine = "2.8.1"
    const val awaitility = "4.0.1"
    const val async_http_client = "2.10.4"

    // ZooKeeper recipes
    const val curator_recipes = "4.2.0"

    //Spring
    const val spring_boot = "2.3.0.RELEASE"
    const val spring = "5.2.1.RELEASE"
    const val spring_cloud = "Hoxton.SR5"
    const val eureka = "2.1.1.RELEASE"
    const val zuul = ""

    //Jackson
    const val jackson = "2.9.9"

    //FIX
    const val release_plugin = "1.3.16"
    const val aggregating_profiler = "1.6.3"
    const val dynamic_property = "2.0.3"
    const val jfix_stdlib = "3.0.0"
    const val reactor = "1.5.2"
    const val crudility2 = "2.0.15"
    const val distributed_job_manager = "1.3.0"
    const val jfix_cucumber = "1.0.12"

    const val testcontainers = "1.11.3"

    //Retrofit
    const val retrofit = "2.6.0"
    const val okhttp_mockwebserver = "3.14.2"

    //MyBatis
    const val mybatis = "3.5.2"
    const val mybatis_spring = "2.0.2"
    const val mybatis_typehandlers = "1.0.2"
    const val mybatis_ehcache = "1.0.0"

    //AspectJ
    const val aspectj_rt = "1.9.4"
    const val aspectj_weaver = "1.9.4"

    //Hazelcast
    const val hazelcast = "3.12.6"
    const val hazelcast_kubernetes = "1.5.2"

    //CSV
    const val java_dbf = "1.10.1"

    //DBF
    const val commons_csv = "1.6"

    //KBDD
    const val jfix_kbdd = "1.0.5"
    const val corounit = "1.0.26"
    const val koin = "2.1.5"

    //Liquibase
    const val liquibase_core = "3.6.3"

    //Security
    const val jjwt = "0.9.1"
}

object Libs {
    //Third party

    //Gradle plugins runtime kotlin
    const val gradle_kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Vers.gradle_kotlin}"
    const val gradle_kotlin_jdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Vers.gradle_kotlin}"
    const val gradle_kotlin_reflect = "org.jetbrains.kotlin:kotlin-reflect:${Vers.gradle_kotlin}"

    //Project runtime kotlin
    const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Vers.kotlin}"
    const val kotlin_jdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Vers.kotlin}"
    const val kotlin_reflect = "org.jetbrains.kotlin:kotlin-reflect:${Vers.kotlin}"


    const val kotlinx_coroutines_core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Vers.kotlin_coroutines}"
    const val kotlinx_coroutines_reactor = "org.jetbrains.kotlinx:kotlinx-coroutines-reactor:${Vers.kotlin_coroutines}"

    const val guava = "com.google.guava:guava:${Vers.guava}"
    const val caffeine = "com.github.ben-manes.caffeine:caffeine:${Vers.caffeine}"


    const val commons_dbcp2 = "org.apache.commons:commons-dbcp2:2.6.0"
    const val postgresql = "org.postgresql:postgresql:42.2.8"
    const val mssql = "com.microsoft.sqlserver:mssql-jdbc:7.2.2.jre11"

    //CSV
    const val commons_csv = "org.apache.commons:commons-csv:${Vers.commons_csv}"

    //DBF
    const val java_dbf = "com.github.albfernandez:javadbf:${Vers.java_dbf}"

    const val dokka_gradle_plugin = "org.jetbrains.dokka:dokka-gradle-plugin:${Vers.dokkav}"
    const val asciidoctor_plugin = "org.asciidoctor:asciidoctor-gradle-plugin:${Vers.asciidoctor}"
    const val jruby_gradle_plugin = "com.github.jruby-gradle:jruby-gradle-plugin:${Vers.jruby_gradle}"
    const val asciidoctor_gems_plugin = "org.asciidoctor:asciidoctor-gradle-jvm-gems:${Vers.asciidoctor_gems}"

    const val log4j = "org.apache.logging.log4j:log4j-api:${Vers.log4j}"
    const val log4j_core = "org.apache.logging.log4j:log4j-core:${Vers.log4j}"
    const val log4j_kotlin = "org.apache.logging.log4j:log4j-api-kotlin:1.0.0"
    const val slf4j_over_log4j = "org.apache.logging.log4j:log4j-slf4j-impl:${Vers.log4j}"

    //Mockito
    const val mockito = "org.mockito:mockito-core:2.28.2"
    const val hamkrest = "com.natpryce:hamkrest:${Vers.hamkrest}"
    const val mockk = "io.mockk:mockk:1.10.0"

    //Junit
    const val junit_api = "org.junit.jupiter:junit-jupiter-api:${Vers.junit}"
    const val junit_params = "org.junit.jupiter:junit-jupiter-params:${Vers.junit}"
    const val junit_engine = "org.junit.jupiter:junit-jupiter-engine:${Vers.junit}"
    const val junit_vintage = "org.junit.vintage:junit-vintage-engine:${Vers.junit}"

    //Awaitility
    const val awaitility = "org.awaitility:awaitility:${Vers.awaitility}"

    // Async http-client
    const val async_http_client = "org.asynchttpclient:async-http-client:${Vers.async_http_client}"

    //kotlintest
    const val kotlintest = "io.kotlintest:kotlintest-runner-junit5:${Vers.kotlintest}"

    //Cucumber
    const val cucumber_allure = "io.qameta.allure:allure-cucumber4-jvm:${Vers.cucumber_allure}"
    const val cucumber_java = "io.cucumber:cucumber-java:${Vers.cucumber_java}"
    const val cucumber_junit = "io.cucumber:cucumber-junit:${Vers.cucumber_java}"
    const val cucumber_picocontainer = "io.cucumber:cucumber-picocontainer:${Vers.cucumber_picocontainer}"

    //Rest-assured
    const val rest_assured = "io.rest-assured:rest-assured:${Vers.rest_assured}"

    const val liquibase_core = "org.liquibase:liquibase-core:${Vers.liquibase_core}"

    const val curator_recipes = "org.apache.curator:curator-recipes:${Vers.curator_recipes}"

    //KBDD
    const val jfix_kbdd = "ru.fix:kbdd:${Vers.jfix_kbdd}"
    const val jfix_corounit_engine = "ru.fix:corounit-engine:${Vers.corounit}"
    const val jfix_corounit_allure = "ru.fix:corounit-allure:${Vers.corounit}"
    const val koin = "org.koin:koin-core:${Vers.koin}"

    //Fakeit
    const val faker = "com.github.javafaker:javafaker:${Vers.faker}"

    //Spring
    const val spring_boot_starter_webflux = "org.springframework.boot:spring-boot-starter-webflux:${Vers.spring_boot}"
    const val spring_boot_starter_web = "org.springframework.boot:spring-boot-starter-web:${Vers.spring_boot}"
    const val spring_context = "org.springframework:spring-context:${Vers.spring}"
    const val spring_jdbc = "org.springframework:spring-jdbc:${Vers.spring}"
    const val spring_boot_test = "org.springframework.boot:spring-boot-starter-test:${Vers.spring_boot}"
    const val spring_boot_actuator = "org.springframework.boot:spring-boot-starter-actuator:${Vers.spring_boot}"
    const val spring_cloud_dependencies = "org.springframework.cloud:spring-cloud-dependencies:${Vers.spring_cloud}"
    const val spring_boot_starter_security = "org.springframework.boot:spring-boot-starter-security"
    const val netflix_eureka_server = "org.springframework.cloud:spring-cloud-starter-netflix-eureka-server:${Vers.eureka}"
    const val netflix_eureka_client = "org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:${Vers.eureka}"
    const val netflix_zuul = "org.springframework.cloud:spring-cloud-starter-netflix-zuul"

    //Jackson
    const val jackson_databind = "com.fasterxml.jackson.core:jackson-databind:${Vers.jackson}"
    const val jackson_jsr310 = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:${Vers.jackson}"
    const val jackson_kotlin = "com.fasterxml.jackson.module:jackson-module-kotlin:${Vers.jackson}"

    //Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Vers.retrofit}"
    const val retrofit_converter_jackson = "com.squareup.retrofit2:converter-jackson:${Vers.retrofit}"
    const val okhttp_mockwebserver = "com.squareup.okhttp3:mockwebserver:${Vers.okhttp_mockwebserver}"

    //MyBatis
    const val mybatis = "org.mybatis:mybatis:${Vers.mybatis}"
    const val mybatis_spring = "org.mybatis:mybatis-spring:${Vers.mybatis_spring}"
    const val mybatis_typehandlers = "org.mybatis:mybatis-typehandlers-jsr310:${Vers.mybatis_typehandlers}"
    const val mybatis_ehcache = "org.mybatis:mybatis-ehcache:${Vers.mybatis_ehcache}"

    //Testcontainers
    const val testcontainers = "org.testcontainers:testcontainers:${Vers.testcontainers}"
    const val testcontainers_postgres = "org.testcontainers:postgresql:${Vers.testcontainers}"
    const val testcontainers_mssql = "org.testcontainers:mssqlserver:${Vers.testcontainers}"
    const val testcontainers_jupiter = "org.testcontainers:junit-jupiter:${Vers.testcontainers}"

    //FIX
    const val release_plugin = "ru.fix:gradle-release-plugin:${Vers.release_plugin}"
    const val aggregating_profiler = "ru.fix:aggregating-profiler:${Vers.aggregating_profiler}"
    const val aggregating_profiler_kotlin = "ru.fix:aggregating-profiler-kotlin:${Vers.aggregating_profiler}"
    const val aggregating_profiler_prometheus = "ru.fix:aggregating-profiler-prometheus:${Vers.aggregating_profiler}"
    const val dynamic_property = "ru.fix:dynamic-property-api:${Vers.dynamic_property}"
    const val dynamic_property_std_source = "ru.fix:dynamic-property-std-source:${Vers.dynamic_property}"
    const val dynamic_property_spring = "ru.fix:dynamic-property-spring:${Vers.dynamic_property}"
    const val dynamic_property_zk = "ru.fix:dynamic-property-zk:${Vers.dynamic_property}"
    const val dynamic_property_jackson = "ru.fix:dynamic-property-jackson:${Vers.dynamic_property}"

    const val stdlib_concurrency = "ru.fix:jfix-stdlib-concurrency:${Vers.jfix_stdlib}"
    const val jfix_stdlib_files = "ru.fix:jfix-stdlib-files:${Vers.jfix_stdlib}"
    const val stdlib_id_generator = "ru.fix:jfix-stdlib-id-generator:${Vers.jfix_stdlib}"
    const val stdlib_socket = "ru.fix:jfix-stdlib-socket:${Vers.jfix_stdlib}"

    const val crudility_webflux2 = "ru.fix:crudility-webflux:${Vers.crudility2}"
    const val crudility_engine2 = "ru.fix:crudility-engine:${Vers.crudility2}"
    const val crudility_postgres2 = "ru.fix:crudility-postgres:${Vers.crudility2}"
    const val distributed_job_manager = "ru.fix:distributed-job-manager:${Vers.distributed_job_manager}"
    const val jfix_cucumber = "ru.fix:jfix-cucumber:${Vers.jfix_cucumber}"

    const val reactor_model = "ru.fix:completable-reactor-model:${Vers.reactor}"
    const val reactor_runtime = "ru.fix:completable-reactor-runtime:${Vers.reactor}"
    const val reactor_graph_kotlin = "ru.fix:completable-reactor-graph-kotlin:${Vers.reactor}"

    //AspectJ
    const val aspectj_rt = "org.aspectj:aspectjrt:${Vers.aspectj_rt}"
    const val aspectj_weaver = "org.aspectj:aspectjweaver:${Vers.aspectj_weaver}"

    //Hazelcast
    const val hazelcast = "com.hazelcast:hazelcast:${Vers.hazelcast}"
    const val hazelcast_tests = "com.hazelcast:hazelcast:${Vers.hazelcast}:tests"
    const val hazelcast_kubernetes = "com.hazelcast:hazelcast-kubernetes:${Vers.hazelcast_kubernetes}"

    //Security
    const val jjwt = "io.jsonwebtoken:jjwt:${Vers.jjwt}"

}
